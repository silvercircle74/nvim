# Theme documentation

This Neovim config comes with its own advanced theme that offers some special features. At the moment, 
other themes may or may not work, but the goal is to improve this situation.

The theme is called **Darkmatter** and provides the following features:

* Three different background tones. *Cold* (a cool medium gray with a slight blue-ish hue), *Warm* (lower 
  color temperature, slightly redd-ish tint) and *Deepdark* (very dark, almost black background)..

* Three levels of color saturation. *Vivid* with high color intensity and contrast and two levels of 
  desaturated colors.

* Two selections for string coloring. Green or Yellow.

* Fully transparent or opaque background - can be changed while Neovim is running.

* Ability to customize color palettes with plugins

* Supports highlighting plugins to define your own highlight groups. Useful for plugins not supported by 
  default.

* Keyboard shortcuts to change theme settings on the fly. No need to restart Neovim.

* Treesitter and LSP semantic tokens support. Support for some 3rd party syntax plugins is included or 
  can be added by theme plugins.

* optionally, sync the background color when using the Kitty terminal.

## Configuration


