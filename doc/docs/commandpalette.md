# The Command Palette

The command palette is some sort of popup that will allow you to select from a wide range of commands. 
Each command has a description, belongs to a category and has one or more keyboard shortcuts assigned.

## Here is how it looks
<figure markdown="span">
  ![Command palette](images/cpalette.png)
  <figcaption><center>The command palette</center></figcaption>
</figure>

The command palette is always available via the keyboard shortcut ++alt+p++. It is provided by the 
command picker plugin 
