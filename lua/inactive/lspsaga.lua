require('lspsaga').setup({
  code_action_lightbulb = { enable = false },
  outline = {
    win_width=36,
    auto_refresh = true,
    auto_preview = false
  },
  symbol_in_winbar = {
    in_custom = true
  }
})
